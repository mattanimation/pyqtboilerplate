#!/usr/bin/env python
"""
@newField description: Description
@newField revisions: Revisions
@newField applications: Applications
@newField dependencies: Dependencies

@author: Matt Murray [@mattanimation]
@organization: Atmos Interactive LLC
@description: This is boilerplate

@revisions:
            -04.28.2014
            
@dependencies:
                None - none

@todo:
        None yet

"""
VERSION = 0.01
APP_NAME = "Boilerplate"
COMPANY = "Atmos Interactive LLC."

#----------------------------------------------------------------------------#
#----------------------------------------------------------------- IMPORTS --#
# Built-in
import sys, os, math, logging

# Third-Party

# UI related
from PyQt4 import QtGui,QtCore

#boilerplate
import utils, uibase, core

logging.basicConfig(level=logging.DEBUG)


#----------------------------------------------------------------------------#
#--------------------------------------------------------------- FUNCTIONS --#
def main():
    """
    This is where the main program execution begins.
    @return: None
    """
    app = QtGui.QApplication(sys.argv)
    app.setStyle('Plastique')
    QtCore.qsrand(QtCore.QTime(0,0,0).secsTo(QtCore.QTime.currentTime()))

    #make a splash screen to show while application initializes
    screendir = os.path.dirname(os.path.abspath('__file__'))
    pixmap = QtGui.QPixmap(os.path.join(screendir,
                                        "Resources/splashscreen.png"))
    splash = QtGui.QSplashScreen(pixmap, QtCore.Qt.WindowStaysOnTopHint)
    splash.setMask(pixmap.mask())
    splash.show()
    splash.showMessage((u'v{0} | Starting...'.format(VERSION)),
                       QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom,
                       QtCore.Qt.white)
    
    app.processEvents()
    widget = PyCommune()
    widget.show()
    #hide splash when done loading
    splash.finish(widget)

    sys.exit(app.exec_())
    

#----------------------------------------------------------------------------#
#----------------------------------------------------------------- CLASSES --#

class Boilerplate(QtGui.QMainWindow):
    """This is my main window for my email client.
    """
    def __init__(self,parent=None):
        QtGui.QMainWindow.__init__(self, parent) # frameless window QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowSystemMenuHint
        #make whole app transparent self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setGeometry(150,150,1280,720)
        self.setWindowTitle("{0} v{1}".format(APP_NAME, VERSION))

        #load styledata
        self.styleData = utils.get_stylesheet()

        #try config file
        self.config = core.UserConfig()
        
        #user data file is stored in c:\\users\matt as .imapqt
        self.USERDATA = userdata.GetUserData()
        
        #create first instance of db if needed
        userdata.create_init_db()
        
        print "userdata {0}".format(self.USERDATA)
        #if ret==1:
        #   QtGui.QApplication.quit()
        #else:
        
        #this will hold all ui elements throughout application
        self.uiWidgets = {}
        self.settingsWindow = None
        
        self.CreateActions()
        self.CreateMenu()

        self.uiWidgets['mailWidget'] = customWidgets.MailWidget(self.USERDATA)
        self.setCentralWidget(self.uiWidgets['mailWidget'])
        self.setStyleSheet(self.styleData)
        
        self.uiWidgets['styledata'] = self.styleData
        
        #load any external plugins here after building core ui?
        pl = core.PluginLoader(self, self.uiWidgets)
        self.plugs = pl.plugins
        
        #test calling a command from plugins
        #pl.test_command("foo")
        

    def CreateMenu(self):
        """
        Create the main menu bar
        """
        self.uiWidgets['fileMenu'] = self.menuBar().addMenu("&File")
        self.uiWidgets['fileMenu'].addAction(self.settingsAction)
        self.uiWidgets['fileMenu'].addSeparator()
        self.uiWidgets['fileMenu'].addAction(self.exitAction)

        self.uiWidgets['itemMenu'] = self.menuBar().addMenu("&Item")
        self.uiWidgets['itemMenu'].addAction(self.deleteAction)
        self.uiWidgets['itemMenu'].addSeparator()
        self.uiWidgets['itemMenu'].addAction(self.toFrontAction)
        self.uiWidgets['itemMenu'].addAction(self.sendBackAction)
        
        self.uiWidgets['plugMenu'] = self.menuBar().addMenu("&Plugins")

        self.uiWidgets['aboutMenu'] = self.menuBar().addMenu("&Help")
        self.uiWidgets['aboutMenu'].addAction(self.aboutAction)

    def CreateActions(self):
        """
        create the actions that are tied to the menu bar
        """
        self.toFrontAction = QtGui.QAction(
            QtGui.QIcon(':/images/bringtofront.png'), "Bring to &Front",
            self, shortcut="Ctrl+F", statusTip="Bring item to front",
            triggered=self.bringToFront)

        self.sendBackAction = QtGui.QAction(
            QtGui.QIcon(':/images/sendtoback.png'), "Send to &Back", self,
            shortcut="Ctrl+B", statusTip="Send item to back",
            triggered=self.sendToBack)

        self.deleteAction = QtGui.QAction(QtGui.QIcon(':/images/delete.png'),
                                          "&Delete", self, shortcut="Delete",
                                          statusTip="Delete item from diagram",
                                          triggered=self.deleteItem)
        
        self.settingsAction = QtGui.QAction("S&ettings", self, shortcut="Ctrl+T",
                                            statusTip="Change Settings",
                                            triggered=self.showSettings)
        

        self.exitAction = QtGui.QAction("E&xit", self, shortcut="Ctrl+X",
                                        statusTip="Quit Scenediagram example",
                                        triggered=self.close)


        self.aboutAction = QtGui.QAction("A&bout", self, shortcut="Ctrl+B",
                                         triggered=self.about)

    def deleteItem(self):
        pass

    def bringToFront(self):
        pass

    def sendToBack(self):
        pass

    def about(self):
        """
        show a small pop-up dialog with info about author/check for updates
        """
        mb = utils.generic_msg("""{0} is a product of \n
        {1} Please Use At Own RISK!""".format(APP_NAME, COMPANY),
                               "info",
                               True)
        #mb.setStyleSheet(self.styleData)
        
    def showSettings(self):
        """
        create settings window
        """
        self.settingsWindow = customWidgets.SettingsWindow(self.USERDATA,
                                                           self.plugs)
        self.settingsWindow.setStyleSheet(utils.get_stylesheet())
        
        self.settingsWindow.show()
        

    def closeEvent(self, event):
        #close Imap connection
        self.centralWidget().messageCenter.EndSession()
        event.accept()
        QtGui.QApplication.quit()




if __name__ == "__main__":
    main()

