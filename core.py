'''
Created on May 3, 2014

@author: Matt
@summary: This module holds all the core functions and classes for sidemail
'''

import imp,os,sys
import imaplib
import email
import re
import userdata
import utils
import ConfigParser
from threading import Thread


import logging

#==============================================================================#
#    THREADS
#==============================================================================#
class FindMessagesThread(Thread):
    """
    This thread is run when searching for email messages
    """
    
    def __init__(self, item, messageCenter, messageDict, messageListBox):
        """
        init
        """
        Thread.__init__(self)
        self.item = item
        self.messageCenter = messageCenter
        self.mDict = messageDict
        self.MessageListBox = messageListBox
        
    def run(self):
        """
        what the main body of the thread will do
        """
        self.messageCenter.SelectBox(str(self.item).split()[-1])
        self.mDict = self.messageCenter.GetMessages()
        displaylist = ["{0}\t{1}\tFrom:{2}".format(a,b.get('subject'),b.get('from')) for (a,b) in self.mDict.items()]
        displaylist.sort(key=lambda x: int(x.split('\t')[0]),reverse=True)
        self.MessageListBox.RefreshList(displaylist)
        print self.mDict
        

#==============================================================================#
#    USER CONFIG
#==============================================================================#
class UserConfig():
    """
    This class will hold all the commands for the current user
    An instance will be created on load, and the data will be stored
    and manipulated in this class then on exit, the data will be saved
    from this class
    """
    
    def __init__(self, rootDir=None):
        """
        init
        """
        rootDir = utils.get_root_url()
        self.configFileName = os.path.join(rootDir, "settings.ini")
        self.settings = {}
        
        #check and see if config has been created, create if not
        if os.path.exists(self.configFileName):
            self.readConfig()
        else:
            self.createConfig()
        
        
    def createConfig(self):
        """
        create the initial config file if doesn't exist
        """
        logging.info("No Config File found, creating new one: {0}".format(self.configFileName))
        config = ConfigParser.ConfigParser()
        config.add_section("Settings")
        config.add_section("Accounts")
        config.add_section("IMAP")
        
        #general settings
        config.set("Settings", "font", "Source Sans Pro")
        config.set("Settings", "font_size", "10")
        config.set("Settings", "theme_style", "midnight")
        
        #user settings
        config.set("Accounts", "name", "No Name")
        
        
        #server settings
        config.set("IMAP", "username", "username")
        config.set("IMAP", "server", "imap.gmail.com")
        config.set("IMAP", "e_mail", "blank@blank.com")
        config.set("IMAP", "password", "test1234")
        
        #save file
        with open(self.configFileName, 'w') as cFile:
            config.write(cFile)
        
        #now the file is created run the readConfig function to set defaults
        self.readConfig()
    
    def readConfig(self):
        """
        read the config file and set settings
        """
        logging.info("Reading config file: {0}".format(self.configFileName))
        #open config
        config = ConfigParser.ConfigParser()
        config.read(self.configFileName)
        
        print(config.items("Settings"))
        
        #set config items to settings dict
        self.settings = {}
        
        for set in ["Settings", "Accounts", "IMAP"]:
            for (k,v) in config.items(set):
                self.settings[k] = v
            
        logging.info("read settings are: {0}".format(self.settings))
        


#==============================================================================#
# PLUGIN CONTENT
#==============================================================================#

class PluginLoader():
    """
    This class handles loading of any external plugins that would extend
    the functionality of the core application
    """
    
    def __init__(self, appref, coreui=None):
        """
        init the class
        @param appref: instance of the main application
        @param coreui: a dict containing the core ui elements
        """
        fileroot = os.path.dirname(os.path.abspath(__file__))
        logging.info(fileroot)
        self.pluginsFolder = os.path.join(os.path.join(fileroot, '..'), 'plugins')
        self.mainModule = "__init__"
        self.loadedPlugins = []
        
        self.coreui = coreui
        self.appref = appref
        
        #load current plugins
        self.plugins = self.get_plugins()
        self.count = len(self.plugins)
        self.launch_plugins()
        
    def get_plugins(self):
        """
        @param None: none
        @return: a list of possible plugins to load
        """
        self.plugins = []
        possiblePlugins = os.listdir(self.pluginsFolder)
        
        #search through plugins folders looking for hopefuls
        for p in possiblePlugins:
            location = os.path.join(self.pluginsFolder, p)
            if not os.path.isdir(location) or not "{0}.py".format(self.mainModule) in os.listdir(location):
                continue
            info = imp.find_module(self.mainModule, [location])
            self.plugins.append({"name": p, "info": info})
            
        return self.plugins
    
    def launch_plugins(self):
        """
        find all plugins and load them into the ui
        
        """
        for p in self.plugins:
            logging.info("loading plugin {0}".format(p["name"]))
            plug = self.load_plugin(p)
            self.loadedPlugins.append(plug)
            plug.init_ui(self.appref, self.coreui) #plug.run to start plugin
    
    def test_command(self, command):
        """
        test running a command in loaded plugins
        """
        logging.info("sent command: {0} to all loaded plugins".format(command))
        for p in self.loadedPlugins:
            #test running command for each plugin
            if command in p.commands.keys():
                p.commands[command]()
    
    def load_plugin(self, plug):
        """
        load the plugin
        
        @param plug: the name of the plugin to load
        @type plug: object | {name: name, info:info}
        """
        return imp.load_module(self.mainModule, *plug["info"])
            



#==============================================================================#
# CORE EMAIL/IMAP CONTENT
#==============================================================================#

class ImapReader():

    def __init__(self, userdata):
        """
        """
        
        #get latest user index and apply to data
        self.currentUserData = userdata['accounts'][userdata['currentaccountindex']]
        logging.info("user is: {0} and pass is: {1} server is {2}".format(self.currentUserData['username'],
                                                                          self.currentUserData['password'],
                                                                          self.currentUserData['server']))
        self.messageLimit = 100
        self.boxes = []
        self.MessDict = {}
        self.MBOX = ""
        try:
            self.M = imaplib.IMAP4_SSL(host=self.currentUserData['server'])
            print self.M
            self.M.login(self.currentUserData['username'],
                         self.currentUserData['password'])
            print "hmmm: {0}".format(self.M.lsub())
            ret,self.boxes = self.M.lsub()
            
        except Exception, e:
            logging.warning("An error occured {0} -- {1}".format(Exception, e))
            
                     

    def SelectBox(self, mname):
        self.M.select(mname)
        self.MBOX = mname

    def GetMessages(self):
        ret_dict = {}
        result, data = self.M.uid('search',None, 'ALL')
        #limit to 10 for now
        i=0
        for num in data[0].split():
            result, data = self.M.uid('fetch', num, '(RFC822)')
            ret_dict[num]=ParseMail(data[0][1])
            i += 1
            if i >= self.messageLimit:
                break
        return ret_dict

    def EndSession(self):
        """
        close session and logout
        """
        
        self.M.close()
        self.M.logout()

    def DeleteMessage(self, num):
        print "deleting number %s from mailbox %s"%(num,self.MBOX)
        self.M.copy((num),'Deleted')
        ok, error = self.M.store(num, 'FLAGS', '(\Deleted)')
        self.M.expunge()

    def MoveMessage(self, num, newbox):
        print "moving number %s to mailbox %s"%(num,newbox)
        self.M.copy((num),newbox)
        ok, error = self.M.store(num, 'FLAGS', '(\Deleted)')
        self.M.expunge()
        print ok

    def BuildContactList(self, udata):
        """
        check to see if the local file has been created for contacts
        if not then read the messages from the users mails and parse the recieved addresses
        compile a list of all the addresses and use that as the base contacts list
        """

        #c = userdata.GetContactsForAccount(udata)
        print "eh ? {0}".format(udata["accounts"][udata["currentaccountindex"]])
        c = userdata.db_get_contacts(udata["accounts"][udata["currentaccountindex"]])
        print "contacts found {0}".format(c)
        if len(c) > 0:
            return c

        """
        email_list=[]
        
        #========HAVE THIS FUNCTION RUN EACH TIME MESSAGES ARE LOADED
        #========AND ONLY ON THE ONES THAT ARE NOT IN THE DB
        #========THAT WAY CONTACTS ARE LOADED ON A MESSAGE BY MESSAGE BASIS

        print self.M.list()
        #folders: [Gmail]/Trash, [Gmail]/Starred, [Gmail]/Spam, [Gmail]/Sent Mail,
        #[Gmail]/Important, [Gmail]/Drafts, [Gmail]/All Mail, [Gmail], INBOX
        currentUser = udata['accounts'][udata['currentaccountindex']]['username']
        
        email_list = self.collect_addresses(currentUser, '[Gmail]/All Mail')

        #save the email-list to the user contact data
        print "Contacts Downloaded!"
        #load current list first and modify that
        currentCList = userdata.UserContacts('read')
        currentCList["allcontacts"][udata['user']] = email_list
        udata.UserContacts("write",currentCList)
        
        #load into sql database
        userdata.db_add_contacts(udata, email_list) """
        return c

    def CollectAddresses(self, cUser, boxname, messages=None):
        """
        Search the selected mailbox for messages and extract the addresses of 
        senders
        @param cUser: string of current username
        @param boxname: a string that is the name of the mailbox to be searched
        @return: a list of email addresses found with associated weights
        """
        emls = []
        contacts = []
        
        if messages == None:
            s = self.M.select(boxname) #sent
    
            result, data = self.M.uid('SEARCH', None, 'ALL')
            ids = data[0]
            id_list = ids.split()
            for i in id_list:
                typ, data = self.M.fetch(i,'(RFC822)')
                for response_part in data:
                    if isinstance(response_part, tuple):
                        msg = email.message_from_string(response_part[1])
                        sender = msg['from'].split()[-1]
                        address = re.sub(r'[<>]','',sender)
                        # Ignore any occurences of own email address and add to list
                        if not re.search(r'' + re.escape(cUser),address):
                            contacts.append(address)
                            if not address in emls:
                                emls.append(address)
                                print address
        else:
            for response_part in messages:
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1])
                    sender = msg['from'].split()[-1]
                    address = re.sub(r'[<>]','',sender)
                    print "Address"
                    # Ignore any occurences of own email address and add to list
                    if not re.search(r'' + re.escape(cUser),address):
                        contacts.append(address)
                        if not address in emls:
                            emls.append(address)
                            print address
        
        #count the number of occurances of each email in contacts list and
        #set weight values accordingly
        print "emls: {0}".format(emls)
        print "contacts: {0}".format(contacts)
        counts = []
        for e in emls:
            newCon = {"email": e, "weight": contacts.count(e)}
            counts.append(newCon)
        
        return counts

    
def ParseMail(stringMess):
    """
    This ain't pretty, I will migrate all imap/email routines to another file
    and then have that d/load and clean up the imap directory of my internet account.
    Then all of my emails on this computer will be stored in sqlite databases.
    To change where I get my email from I will just have to change the message center.
     Hopefully this data model will be consistent.  
    This function will recieve a message and return a one element dictionary
    {subject:subject,to:To,from:sender,date:date,body:main content,attachments:[(filename,type,Data)]
    """
    BODY_MIME = ['text/html','text/plain']
    mailmess = email.message_from_string(stringMess)
    mess_dict = {'subject':mailmess.get('subject'),
            'to':mailmess.get('To'),
            'from':mailmess.get('From'),
            'date':mailmess.get('Date')}
    attachments = []
    bodies =[]
    utils.GetPayloads(bodies,attachments,mailmess)
    mess_dict['body'] = bodies
    mess_dict['attachments']=attachments
    return mess_dict


    
#==============================================================================#
# OTHER CONTENT
#==============================================================================#
    
