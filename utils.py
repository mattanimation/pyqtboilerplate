'''
Created on May 3, 2014

@author: Matt
@summary: This Module holds all the utility functions and classes for sidemail
'''

from PyQt4 import QtGui
import ConfigParser
import os,sys, base64
import random
# import code for encoding urls and generating md5 hashes
import urllib, hashlib
import logging
import json
import sqlite3 as lite
import hashlib




#==============================================================================#
# System RELATED
#==============================================================================#
def createConfig(path):
    """
    Create a config file
    """
    config = ConfigParser.ConfigParser()
    config.add_section("Settings")
    config.set("Settings", "Accounts", [{"name":"Matt"}])
    config.set("Settings", "Image","derp.png")

    with open(path, "wb") as config_file:
        config.write(config_file)


def crudConfig(path):
    """
    Create, read, update, delete config
    """
    if not os.path.exists(path):
        createConfig(path)

    config = ConfigParser.ConfigParser()
    config.read(path)

    #read some values
    name = config.get("Settings", "Name")
    image = config.get("Settings", "Image")

    #change a value in the config
    config.set("Settings", "Name", "Derp")

    #delete value from config
    config.remove_option("Settings", "Image")

    #write over config file
    with open(path, "wb") as config_file:
        config.write(config_file)
        
        
def get_root_url():
    """
    return the base path to these modules
    """
    return os.path.dirname(os.path.abspath('__file__'))

def GetPayloads(body, att, mmess):
    """
    this function is going to decide if something is a payload or a list of payloads.
    It will call itself recursively and it will modify body and att in place.
    """
    cpay = mmess.get_payload()
    if type(cpay) is list:
        for item in cpay:
            GetPayloads(body, att, item)
    else:
        tencode = mmess.get('Content-Transfer-Encoding','printed-quotable')
        params = dict(mmess.get_params([('text/plain','')]))
        mtype = mmess.get_content_type()
        if params.has_key('name'):
            att.append((params.get('name'),mtype,cpay,tencode))
        else:
            body.append((mtype,UCode(cpay,params.get('charset','utf-8'))))

def UCode(mystring, mycode):
    try:
        ret_string = unicode(mystring,mycode,'ignore')
    except:
        print 'user encoding failed'
        ret_string = unicode(mystring,'utf-8','ignore')
    return ret_string

def BinaryDecode(enc, data):
    """
    This is mainly going to run the base64.dencodestring() routine, but I am writing it here
    so that I don't have to deal with it there.  And so that all of my email routines are right here
    """
    if enc=='base64':
        try:
            return base64.decodestring(data)
        except:
            return u"file is not base64, claims to be:%s\n%s"%(enc,data)
    else:
        return data
        
        
def STMPMailFormat(add):
    """
    @param add: str or list of addresses to have <> wrapped around
    @return add: str or list
    """
    ret = ""
    if not isinstance(add, basestring):
        for ad in add.split(','):
            ret += "<{0}>,".format(ad)
    else:
        ret = "<{0}>".format(add)
        
    return ret
    
def create_unique_table_name(salt):
    """
    create a unique name to use to name the table that will be created to
    store the users particular email messages
    The name MUST ALWAYS start with a non number to be a valid table name
    @param salt: the base key to alter and create a new name
    """
    ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    uname = 'a'.join(random.choice(ALPHABET) for i in range(len(salt) * 2))
    logging.info("created new unique name for {0} , it's: {1}".format(salt,
                                                                      uname))
    return uname
        
#==============================================================================#
# UI RELATED
#==============================================================================#
def get_stylesheet():
    """
    Load the stylesheet for the application
    
    @return dat: the contents of the stylesheet
    """
    f=open(os.path.join(get_root_url(),'PyCommune\\Resources\\sidemail.stylesheet'), 'r')
    dat = f.read()
    f.close()
    return dat

def get_resources_path():
    """
    @return pth: the string path of the users image file
    """
    return os.path.join(get_root_url(),'PyCommune/Resources')

def get_user_image_path():
    """
    @return pth: the string path of the users image file
    """
    return os.path.join(get_resources_path(),'me.png')

def get_gravatar_path(eml):
    """
    @param eml: email address to use to find gravatar
    @return pth: the str path to gravatar image
    """
    d = os.path.join(get_resources_path(), 'default.png')
    size = 40
    
    p1 = hashlib.md5(eml.lower()).hexdigest()
    #print("eml is : {0} and hsh is {1}".format(eml, p1))
    p2 = urllib.urlencode({'s':str(size)})
    pth = "http://www.gravatar.com/avatar/{0}?{1}".format(p1,p2)
    #print(pth)
    return urllib.urlopen(pth)

def get_favicon_path(eml):
    """
    @param eml: email address to use to find gravatar
    @return pth: the str path to gravatar image
    """
    pth = "http://www.{0}/favicon.ico".format(eml.split("@")[1])
    
    return urllib.urlopen(pth)

def QPixFromPath(pth):
    """
    @param pth: the image path to load
    @return: QPixmap that has started to load image 
    """
    qp = QtGui.QPixmap()
    if 'http' in pth:
        qp.loadFromData(urllib.urlopen(pth).read())
    else:
        qp.load(pth)
    return qp
    
def generic_msg(msg, msg_type, dialog=False):
    """
    generic_msg is a helper function to alert the user to either an
    information dialog or an info dialog
    
    @type msg: str
    @param msg: The message to be displayed
    @type msg_type: str
    @param msg_type: The message type. Valid types are info, warning and error
    @type dialog: bool
    @param dialog: Display a dialog or not. Default is False
     
    """
    # set as lower case in case the type has a caps letter in it
    msg_type = str.lower(msg_type)
    if msg_type == 'error' or msg_type == 'warning':
        sys.stderr.write('%s\n' % msg)
    else:
        sys.stdout.write('%s\n' % msg)
    
    res = None
    if dialog:
        if msg_type == 'error':
            res = QtGui.QMessageBox.critical(None, 'Error', str(msg))
        elif msg_type == 'warning':
            res = QtGui.QMessageBox.warning(None, 'Warning', str(msg))
        elif msg_type == 'info':
            res = QtGui.QMessageBox.information(None, 'Information', str(msg))
        elif msg_type == 'quest':
            res = QtGui.QMessageBox.question(None, 'Question', str(msg))
    return res



class UserLoader(QtGui.QDialog):

    def __init__(self,parent=None,server=None,user=None,password=None,email_address=None,f=QtCore.Qt.WA_DeleteOnClose):

        QtGui.QDialog.__init__(self,parent)
        self.setWindowTitle("Get Login Info")
        layout = QtGui.QGridLayout()
        layout.addWidget(QtGui.QLabel("Server(imap.gmail.com): ",self),0,0)
        self.iServer = QtGui.QLineEdit(self)
        if server:
            self.iServer.setText(server)
        layout.addWidget(self.iServer,0,1)
        layout.addWidget(QtGui.QLabel("User: ",self),1,0)
        self.iLogin = QtGui.QLineEdit(self)
        if user: self.iLogin.setText(user)

        layout.addWidget(self.iLogin,1,1)
        layout.addWidget(QtGui.QLabel("Email Address: ",self),3,0)
        self.iAddress = QtGui.QLineEdit(self)
        layout.addWidget(self.iAddress,3,1)

        if email_address: self.iAddress.setText(email_address)

        layout.addWidget(QtGui.QLabel("Password: ",self),2,0)
        self.iPassword = QtGui.QLineEdit(self)
        self.iPassword.setEchoMode(QtGui.QLineEdit.Password)

        if password: self.iPassword.setText(password)

        layout.addWidget(self.iPassword,2,1)
        self.Check = QtGui.QPushButton("Check Mail",self)
        layout.addWidget(self.Check,4,0)

        self.connect(self.Check,QtCore.SIGNAL("clicked()"),self.Complete)
        self.setLayout(layout)

    def getText(self):
        self.setModal(True)
        retcode = self.exec_()
        return {'currentaccountindex':0,'accounts':[{'server':str(self.iServer.text()),
                          'username':str(self.iLogin.text()),
                          'email_address':str(self.iAddress.text()),
                          'password':str(self.iPassword.text())}]},retcode

    def Complete(self):
        if not self.iServer.text() == "":
            self.done(0)
        else:
            QtGui.QMessageBox.warning(self,
                                      "Ooops!",
                                      "You need to at least set a servername",
                                      QtGui.QMessageBox.Ok)

    def closeEvent(self,e):
        self.done(1)

def get_db_name():
    """
    @return dbname: the name of the core database for all users
    """
    path = os.path.join(get_root_url(), 'PyCommune/data/')
    dbname = os.path.join(path,'pycommune.db')
    #print "in this case the root would be {0}".format(dbname)
    return dbname

def create_init_db():
    """
    create the first db
    """
    dbname= get_db_name()
    if not os.path.isfile(dbname):
        print("creating db at {0}".format(dbname))
        conn = lite.connect(dbname)
    
        #create table
        cursor = conn.cursor()
        cursor.execute("""CREATE TABLE accounts
                        (name VARCHAR, 
                        email VARCHAR, 
                        username VARCHAR, 
                        password VARCHAR, 
                        server VARCHAR, 
                        ssl INT, 
                        avatar VARCHAR, 
                        port INT, 
                        description VARCHAR, 
                        mailtable VARCHAR, 
                        contactstable VARCHAR)
                        """)
    else:
        print "created first db"
        
        
def db_query_folders(dat):
    """
    look at a users data for specific set of data
    """
    dbname = get_db_name()
    conn = lite.connect(dbname)
    
    #get mailtable
    mailTable = dat['mailTable']
    print ("table to check is " + mailTable)
    cursor = conn.cursor()
    
    qCmd = "SELECT * FROM {0} WHERE folder != null".format(mailTable)
    cursor.execute(qCmd)
    res = cursor.fetchall()
    print res
    
    return res

def db_add_messages(dat, mList, fName):
    """
    for each item in the list add to the folders column
    @param dat: a dict containing the users info
    @param mList: the list of messages to add a json string that was a dict
    @param fName: the name of the folder the messages are assigned to
    """
    dbname = get_db_name()
    conn = lite.connect(dbname)
    cursor = conn.cursor()
    
    #first look at what is in db now and compare to incoming list
    #only add what isn't already there
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    oldMessages = db_get_messages(dat, fName)
    print "old messages {0}".format(oldMessages)
    print "mlist {0}".format(mList)
    
    newList = []
    
    if oldMessages != None:
        for k,v in oldMessages.iteritems():
            for m in mList:
                print m['subject']
                print oldMessages[k]['subject']
                if m["subject"] == oldMessages[k]['subject']:
                    newList.append(m)
    else:
        newList = mList
    
    print "mlist is {0}".format(mList)
    print "new list {0}".format(newList)
    
    
    vals = []
    for m in newList:
        #message, username, folder
        vals.append((m, dat['username'], fName))
    
    print "vals is {0}".format(vals)
    qCmd = "INSERT INTO {0} VALUES(?,?,?)".format(dat['mailTable'])
    print qCmd
    cursor.executemany(qCmd, vals)
    
    #save
    conn.commit()
    
    logging.info("added new messages to table")
    
def db_get_messages(dat, fName):
    """
    get messages from database and return as a dict, {num, {data}}
    return None if no match is found
    @param dat: user data dict
    @param fName: folder name where messages are from
    @return rDict {num, {data}}
    """
    rDict = {}
    
    dbname = get_db_name()
    conn = lite.connect(dbname)
    cursor = conn.cursor()
    
    qCmd = "SELECT * FROM {0} WHERE folder = '{1}'".format(dat['mailTable'],
                                                         fName)
    cursor.execute(qCmd)
    res = cursor.fetchall()
    
    if len(res) > 0:
        for i in range(len(res)):
            #print res[i][0]
            rDict.setdefault(i, json.loads(str(res[i][0])))
            
        #logging.info("rdict created: {0}".format(rDict))
        return rDict
    else:
        return None
    
        
def db_add_acct(dat):
    """
    add a new user account to the database and create new tables for the
    mail and contacts of that user
    @param dat: a dict containing the users default info on the account
    """
    dbname = get_db_name()
    conn = lite.connect(dbname)
    
    name = dat['name']
    uName = dat['username']
    eml = dat['email_address']
    pw = dat['password']
    svr = dat['server']
    avtr = dat['avatar']
    ssl = dat['ssl']
    port = dat['port']
    desc = dat['description']
    
    mTbl = create_unique_table_name(uName)
    cTbl = create_unique_table_name(name)
    
    #make sure password is stored encrypted
    encPwd = hashlib.sha256()
    encPwd.update(pw)
    encPwd.update(uName)
    encPwd.hexdigest()
    
    cursor = conn.cursor()
    #name, email, username, password, server, ssl, avatar, port, desc, mtbl, ctbl
    sqlInsert = 'INSERT INTO accounts VALUES ("{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}", "{8}", "{9}", "{10}")'.format(name,
                                                                                                                                eml,
                                                                                                                                uName,
                                                                                                                                encPwd,
                                                                                                                                svr,
                                                                                                                                ssl,
                                                                                                                                avtr,
                                                                                                                                port,
                                                                                                                                desc,
                                                                                                                                mTbl,
                                                                                                                                cTbl)
    cursor.execute(sqlInsert)
    
    #create the new mail table and contacts table
    cursor.execute('CREATE TABLE {0} (message VARCHAR, username VARCHAR, folder VARCHAR)'.format(mTbl))
    cursor.execute('CREATE TABLE {0} (email VARCHAR, name VARCHAR, weight INT, username VARCHAR, twitter VARCHAR)'.format(cTbl))
    
    #save
    conn.commit()
    logging.info("creation of new database entry for {0} complete.".format(name))
    
    
def db_update_acct(data):
    """
    update the database with this new user info
    @param data: a dict containing all user acct data
    """
    conn = lite.connect(get_db_name())
    cursor = conn.cursor()
    
    eml = data['email_address']
    for k,v in data.iteritems():
        if not k == 'email_address':
            sqlCmd = 'UPDATE accounts SET {0} = "{1}" WHERE email = "{3}"'.format(k,v,eml)
            cursor.execute(sqlCmd)
    
    conn.commit()
    logging.info("update of database entry for {0} complete.".format(data['name']))
    
def db_remove_acct(data):
    """
    remove the incoming data of acct from db and coresponding mail and contact
    tables
    @param data: a dict containing all user acct data
    """
    pass
    
    
def db_add_contacts(data, allContacts):
    """
    save the contacts into the ctable of the user
    @param data: a dict containing all acct data
    @param allContacts: a list holding all user emails and weights dicts
    """
    conn = lite.connect(get_db_name())
    cursor = conn.cursor()
    
    #get table name
    #print data
    tblName = data['contactsTable']
    #cursor.execute('SELECT contactstable FROM "accounts" WHERE email = "{0}"'.format(data['email_address'])).fetchone()
    
    #first get data from db and check against incoming list
    #any values that aren't already in db will not be included
    currentContacts = db_get_contacts(data)
    print "current contacts: {0}".format(currentContacts)
    print "new contacts: {0}".format(allContacts)
    
    currentEmails = [e['email'] for e in currentContacts]
    newEmails = [e["email"] for e in allContacts]
    contactsToAdd = [con for con in newEmails if con not in currentEmails]
    
    #if email is in contacts and in new contacts, then just add to that contacts
    # weight value
    for con in contactsToAdd:
        for ac in allContacts:
            for cc in currentContacts:
                if ac['email'] == cc['email']:
                    cc['weight'] += ac['weight']
            if ac['email'] == con:
                newCon = {"email": con, "weight": ac['weight']}
                currentContacts.append(newCon)
                 
    
    
    # if email is not in contacts then add the email AND weight to new list

    if len(currentEmails) < 1:
        currentContacts = allContacts
    
    #dump contacts into table
    logging.info("adding {0} to {1}".format(currentContacts, tblName))
    vals = []
    for ct in currentContacts:
        #email name weight username
        vals.append((ct['email'], 'no name', ct['weight'], data['username']))
    cursor.executemany('INSERT INTO {0} VALUES(?,?,?,?)'.format(tblName), vals)
    
    conn.commit()
    logging.info("contacts stored!")
    
    
def db_get_contacts(data):
    """
    
    @param dat: user data dict
    @return res: contact list
    """
    
    conn = lite.connect(get_db_name())
    cursor = conn.cursor()
    
    print data
    qCmd = "SELECT * FROM {0}".format(data['contactsTable'])
    print "get contacts cmd: {0}".format(qCmd)
    cursor.execute(qCmd)
    res = cursor.fetchall()
    
    print res
    ret = []
    for r in res:
        con = {}
        con.setdefault("email", r[0])
        con.setdefault("name", r[1])
        con.setdefault("weight", r[2])
        ret.append(con)
        
    
    if len(res) > 0:
        return ret
    else:
        return []
    
def CurrentUser(data):
    """
    @return current user dict
    """
    return data['accounts'][data['currentaccountindex']]

def AddNewAcct(data):
    """
    add new account to the db and save
    """
    #get current data
    d = GetUserData()
    d['accounts'].append(data)
    path = os.path.expanduser("~")
    fname = os.path.join(path,'.imapqt')
    WriteUser(fname,d)
    
def UpdateAcct(data):
    """
    use the incoming data to update the acct data
    """
    path = os.path.expanduser("~")
    fname = os.path.join(path,'.imapqt')
    d = GetUserData()
    #find email in account data and update that index of the array
    i=0
    for acct in d['accounts']:
        if data['email_address'] == acct['email_address']:
            continue
        i+=1
    d['accounts'][i] = data 
    WriteUser(fname,d)
    
def RemoveAcctWithEmail(eml):
    """
    find the account with this email in it and remove it
    """
    print "attempting to remove {0} from data".format(eml)
    d = GetUserData()
    i=0
    for acct in d['accounts']: 
        if acct['email_address'] == eml:
            del d['accounts'][i]
            path = os.path.expanduser("~")
            fname = os.path.join(path,'.imapqt')
            WriteUser(fname,d)
            return 1
        i+=1
    return 0

def GetAcctDataByEmail(eml):
    """
    return the user dict that has matching email
    """
    d = GetUserData()
    for acct in d['accounts']: 
        if acct['email_address'] == eml:
            return acct
    return None

def GetUserData():
    path = os.path.expanduser("~")
    fname = os.path.join(path,'.imapqt')
    try:
        data = ReadUser(fname)
        return data
    except:
        print "couldn't get user data"
        UserData()

def UserData(parent=None):
    """
    read or write the userdata to a file
    and load to input window
    @param: parent - None
    """
    path = os.path.expanduser("~")
    fname = os.path.join(path,'.imapqt')
    try:
        data = ReadUser(fname)
    except:
        data = {}
    text,ret = UserLoader.getText(UserLoader(**data))
    if ret == 0:
        try:
            WriteUser(fname,text)
        except:
            print "could not write file"
        return text,ret

def GetContactsForAccount(uData):
    """
    return the contacts list for the given user
    @param uData: the dict of the loaded user data
    """
    ind = int(uData['currentaccountindex'])
    #userToFind = uData['accounts'][ind]['username']
    #userToFind = str(userToFind)
    
    conTbl = uData['accounts'][ind]
    return db_get_contacts(conTbl)
    
    #try:
    #    return UserContacts('read')['allcontacts'][userToFind]
    #except Exception, e:
    #    logging.warn("{0} {1}".format(Exception, e))
    #    return ["none"]
    

def UserContacts(do, ddict={}):
    """
    @param: do - string - read or write
    @param: ddict - dict - the data dict {"contacts":[]}
    """
    path = os.path.expanduser("~")
    fname = os.path.join(path,'.imapqtcontacts')

    if do =="read":
        data = {"allcontacts":{"username":["none@gmail.com"]}}
        try:
            data = ReadUser(fname)
        except Exception, e:
            logging.warning("{0} {1}".format(Exception, e))
            data = {"allcontacts":{"username":["none@gmail.com"]}}
        return data
    if do == "write":
        try:
            WriteUser(fname,ddict)
        except:
            print "could not write file"


def ReadUser(fname):
    """
    open and read in user data as json dict
    @param: fname - string, filename
    """
    with open(fname, 'r') as jf:
        jData = json.load(jf)
    return jData

def WriteUser(fname, ddict):
    """
    @param: fname - string, filename
    @param: ddict - dict, userdata
    """
    with open(fname, 'w') as jf:
        jf.write(json.dumps(ddict))
    return 0



